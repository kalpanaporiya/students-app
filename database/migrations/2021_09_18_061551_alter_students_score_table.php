<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterStudentsScoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::table('students_score', function (Blueprint $table) {
            $table->string('english')->default(0)->after('class_id');
            $table->string('maths')->default(0)->after('english');
            $table->string('science')->default(0)->after('maths');
            $table->string('social_sciences')->default(0)->after('science');
            $table->string('computer_basics')->default(0)->after('social_sciences');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students_score', function (Blueprint $table) {            
            $table->dropColumn('english');
            $table->dropColumn('maths');
            $table->dropColumn('science');
            $table->dropColumn('social_sciences');
            $table->dropColumn('computer_basics');
        });        
    }
}
