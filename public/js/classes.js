$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});
if ($("#dt-classes").length > 0) {
    $('#dt-classes').DataTable({
        processing: true,
        serverSide: true,
        ajax: routes.get_classes,
        columns: [
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex'
            },
            {
                data: 'class_name',
                name: 'class_name',
            },
        ]
    });
}