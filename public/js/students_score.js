$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});

$(document).on('change', '#student_id', function () {
    if ($(this).val()) {
        var id = $(this).val();
        $.ajax({
            url: routes.get_student,
            method: "POST",
            async: false,
            data: { id: id },
            success: function (response) {
                if (response.status == true) {
                    $('#frm-students-score #roll_no').val(response.data.roll_no);
                    $('#frm-students-score #roll_no').focus();
                    $('#frm-students-score #student_name').val(response.data.student_name);
                    $('#frm-students-score #student_name').focus();
                    $('#frm-students-score #classes_id').val(response.data.class.class_name);
                    $('#frm-students-score #classes_id').focus();
                } else {
                    toastr["error"](response.message);
                }
            }, error: function (err) {
                toastr["error"]("Something Went Wrong Here!");
            }
        });
    }
});

if ($("#frm-students-score").length > 0) {
    $('select[required]').css({
        position: 'absolute',
        display: 'inline',
        height: 0,
        padding: 0,
        border: '1px solid rgba(255,255,255,0)',
        width: 0
    });
    $("#frm-students-score").validate({
        rules: {
            student_id: {
                required: true,
            },
            roll_no: {
                required: true,
            },
            student_name: {
                required: true,
            },
            classes_id: {
                required: true,
            },
            english: {
                required: true,
                min: 0,
                max: 100,
            },
            maths: {
                required: true,
                min: 0,
                max: 100,
            },
            science: {
                required: true,
                min: 0,
                max: 100,
            },
            social_sciences: {
                required: true,
                min: 0,
                max: 100,
            },
            computer_basics: {
                required: true,
                min: 0,
                max: 100,
            },
        },
        errorElement: 'div',
        errorPlacement: function (error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $.ajax({
                url: $(form).attr('action'),
                method: "POST",
                async: false,
                data: new FormData(form),
                processData: false,
                contentType: false,
                success: function (response) {
                    if (response.status == true) {
                        toastr["success"](response.message);
                    } else {
                        toastr["error"](response.message);
                    }
                    clearUploadForm(form);
                    $('#dt-students-score').DataTable().ajax.reload();
                }, error: function (err) {
                    toastr["error"]("Something Went Wrong Here!");
                }
            });
            return false;
        }
    });

    function clearUploadForm(form = '') {
        $('#md_students_score').modal('close');
        $(form).trigger("reset");
    }

    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-z]+$/i.test(value);
    }, "Letters only please");

    $('#dt-students-score').DataTable({
        processing: true,
        serverSide: true,
        ajax: routes.get_studentdata,
        columns: [
            {
                data: 'student_image',
                name: 'student_image',
                sortable: false,
            },
            {
                data: 'class_name',
                name: 'class_name'
            },
            {
                data: 'roll_no',
                name: 'roll_no'
            },
            {
                data: 'student_name',
                name: 'student_name'
            },
            {
                data: 'english',
                name: 'english'
            },
            {
                data: 'maths',
                name: 'maths'
            },
            {
                data: 'science',
                name: 'science'
            },
            {
                data: 'social_sciences',
                name: 'social_sciences'
            },
            {
                data: 'computer_basics',
                name: 'computer_basics'
            },
        ]
    });
}