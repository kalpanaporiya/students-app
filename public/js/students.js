$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});
if ($("#frm-students").length > 0) {
    $('select[required]').css({
        position: 'absolute',
        display: 'inline',
        height: 0,
        padding: 0,
        border: '1px solid rgba(255,255,255,0)',
        width: 0
    });
    $("#frm-students").validate({
        rules: {
            classes_id: {
                required: true,
            },
            student_name: {
                required: true,
            },
            student_photo: {
                required: true,
            },
        },
        errorElement: 'div',
        errorPlacement: function (error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $.ajax({
                url: $(form).attr('action'),
                method: "POST",
                async: false,
                data: new FormData(form),
                processData: false,
                contentType: false,
                success: function (response) {
                    if (response.status == true) {
                        toastr["success"](response.message);
                    } else {
                        toastr["error"](response.message);
                    }
                    clearUploadForm(form);
                    $('#dt-students').DataTable().ajax.reload();
                }, error: function (err) {
                    toastr["error"]("Something Went Wrong Here!");
                }
            });
            return false;
        }
    });

    function clearUploadForm(form = '') {
        $('#md_students').modal('close');
        $(form).trigger("reset");
    }

    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-z]+$/i.test(value);
    }, "Letters only please");

    $('#dt-students').DataTable({
        processing: true,
        serverSide: true,
        ajax: routes.get_students,
        columns: [
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex'
            },
            {
                data: 'student_image',
                name: 'student_image',
                sortable: false,
            },
            {
                data: 'class_name',
                name: 'class_name'
            },
            {
                data: 'roll_no',
                name: 'roll_no'
            },
            {
                data: 'student_name',
                name: 'student_name'
            },
        ]
    });
}