<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Classes;
use App\Models\Students;
use DB;

class DataController extends Controller
{

    /**
     * @name retrieve_list_of_records
     * @param Request $request
     * @return json array
     * @author KALPANA
     */
    public function getstudents(Request $request)
    {
        try {
            $students = Students::leftjoin('students_score', 'students_score.student_id', '=', 'students.id');
            $select = ['students.id', 'students.student_name', 'students.roll_no', DB::raw('IFNULL(((SUM(students_score.english) + SUM(students_score.maths) + SUM(students_score.science) + SUM(students_score.social_sciences) + SUM(students_score.computer_basics)) / 5),0)  AS total_score'), 'students.classes_id', DB::raw('CONCAT("' . env('ASSET_URL') . '/uploads/students_images/", students.student_photo) AS student_photo')];
            $fieldSet = ['student_name', 'roll_no', 'classes_id'];
            $custom_fields = ['total_score', 'student_photo'];

            #---- Select Statements
            if (!empty($request->data)) {
                $select = ['students.id'];
                $param = explode(',', $request->data);
                foreach ($param as $field) {
                    if (in_array($field, $fieldSet)) {
                        array_push($select, $field);
                    } else if (in_array($field, $custom_fields)) {
                        switch ($field) {
                            case 'student_photo':
                                array_push($select, DB::raw('CONCAT("' . env('ASSET_URL') . '/uploads/students_images/", students.student_photo) AS student_photo'));
                                break;
                        }
                    }
                }
                array_push($select, DB::raw('IFNULL(((SUM(students_score.english) + SUM(students_score.maths) + SUM(students_score.science) + SUM(students_score.social_sciences) + SUM(students_score.computer_basics)) / 5),0)  AS total_score'));
                $students->select($select);
            }

            $students->select($select);
            $students->with('subjects_score');

            #--- filter Statements
            $fields_filter = $request->all();
            if ($fields_filter) {
                array_push($fieldSet, 'id');
                foreach ($fields_filter as $field => $value) {
                    if (in_array($field, $fieldSet)) {
                        if ($field == 'id')
                            $students->where('students.id', $value);
                        else
                            $students->where($field, $value);
                    }
                }
            }
            $students->groupby('students.id');
            $students->orderby('total_score', 'DESC');
            $response = $students->get();
            return response()->json(compact('response'), 200);
        } catch (Exception $e) {
            $response = $e->getMessage();
            return response()->json(compact('response'), 500);
        }
    }
}
