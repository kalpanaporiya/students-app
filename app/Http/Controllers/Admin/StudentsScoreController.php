<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Classes;
use App\Models\Students;
use App\Models\Subjects;
use App\Models\StudentsScore;
use Auth;
use Validator;
use DataTables;

class StudentsScoreController extends Controller
{

    /**
     * @name index
     * @author KALPANA
     */
    public function index()
    {
        $data['students_data'] = Students::get();
        return view("admin.students_score.index", $data);
    }

    /**
     * @name get students data
     * @author KALPANA
     */
    public function get_students_score(Request $request)
    {
        if ($request->ajax()) {
            $data = StudentsScore::with('class', 'student')->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('class_name', function ($row) {
                    return $row->class->class_name;
                })
                ->addColumn('student_name', function ($row) {
                    return $row->student->student_name;
                })
                ->addColumn('roll_no', function ($row) {
                    return $row->student->roll_no;
                })
                ->addColumn('student_image', function ($row) {
                    $actionBtn = '<img src="' . asset('uploads/students_images/' . $row->student->student_photo) . '" height="50px" width="50px"/>';
                    return $actionBtn;
                })
                ->rawColumns(['student_image', 'class_name','roll_no'])
                ->make(true);
        }
    }

    /**
     * @name add new students
     * @author KALPANA
     */
    public function add(Request $request)
    {
        $data['status'] = false;
        $data['message'] = 'Something went wrong here!';
        if ($request->ajax()) {
            $rules = array(
                'student_id' => 'required',
                'classes_id' => 'required',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $data['message'] = 'Please fill the form as per the given instructions.';
            } else {
                $class_id = 0;
                $classData = Classes::where(['class_name' => $request->classes_id])->first();
                if (!empty($classData)) {
                    $class_id = $classData->id;
                }
                $score = new StudentsScore();
                $score->student_id  = $request->student_id;
                $score->class_id  = $class_id;
                $score->english  = $request->english;
                $score->maths  = $request->maths;
                $score->science  = $request->science;
                $score->social_sciences  = $request->social_sciences;
                $score->computer_basics  = $request->computer_basics;
                $score->save();
                if (!empty($score)) {
                    $data['status'] = true;
                    $data['message'] = 'Score Data saved successfully!';
                }
            }
        }
        return response()->json($data);
    }

    public function get_studentdata(Request $request)
    {
        $data['status'] = false;
        $data['message'] = 'Something went wrong here!';
        if ($request->ajax()) {
            $students_data = Students::with('class')->where(['id' => $request->id])->first();
            if ($students_data) {
                $data['status'] = true;
                $data['data'] = $students_data;
                $data['message'] = 'Get student data successfully!';
            }
        }
        return response()->json($data);
    }
}
