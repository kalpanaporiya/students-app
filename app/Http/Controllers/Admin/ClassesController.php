<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Classes;
use App\Models\Students;
use DataTables;

class ClassesController extends Controller
{

    /**
     * @name index
     * @author KALPANA
     */
    public function index()
    {
        return view("admin.classes.index");
    }

    /**
     * @name get class Data
     * @author KALPANA
     */
    public function get_classes(Request $request)
    {
        if ($request->ajax()) {
            $data = Classes::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->make(true);
        }
    }
}
