<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Classes;
use App\Models\Students;
use Auth;
use Validator;
use DataTables;
use Cviebrock\EloquentSluggable\Services\SlugService;
use DB;

class StudentsController extends Controller
{

    /**
     * @name index
     * @author KALPANA
     */
    public function index()
    {
        $data['classes_data'] = Classes::get();
        return view("admin.students.index", $data);
    }

    /**
     * @name get students data
     * @author KALPANA
     */
    public function get_students(Request $request)
    {
        if ($request->ajax()) {
            $data = Students::with('class')->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('class_name', function ($row) {
                    return $row->class->class_name;
                })
                ->addColumn('student_image', function ($row) {
                    $actionBtn = '<img src="' . asset('uploads/students_images/' . $row->student_photo) . '" height="50px" width="50px"/>';
                    return $actionBtn;
                })
                ->rawColumns(['student_image', 'class_name'])
                ->make(true);
        }
        // return response()->json($data);
    }

    /**
     * @name add new students
     * @author KALPANA
     */
    public function add(Request $request)
    {
        $data['status'] = false;
        $data['message'] = 'Something went wrong here!';
        if ($request->ajax()) {
            $rules = array(
                'classes_id' => 'required',
                'student_name' => 'required',
                'student_photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $data['message'] = 'Please fill the form as per the given instructions.';
            } else {
                $imageName = time() . '.' . $request->student_photo->extension();
                $request->student_photo->move(public_path('uploads/students_images'), $imageName);
                $Students = new Students();
                $Students->roll_no = SlugService::createSlug(Students::class, 'roll_no', 'STUDENT' . date('Y') . '00');
                $Students->classes_id = $request->classes_id;
                $Students->student_name = $request->student_name;
                $Students->student_photo = $imageName;

                $Students->save();


                $data['status'] = true;
                $data['message'] = 'Student saved successfully!';
            }
        }
        return response()->json($data);
    }
}
