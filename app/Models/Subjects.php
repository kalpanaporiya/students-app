<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subjects extends Model
{
    protected $table = 'subjects';
    public $timestamps = false;
    protected $fillable = [
        'subject_name'
    ];
}
