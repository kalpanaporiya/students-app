<?php


namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\Classes;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    use Sluggable;

    protected $table = 'students';
    public $timestamps = false;
    protected $fillable = [
        'roll_no', 'classes_id', 'student_name', 'student_photo'
    ];

    public function sluggable()
    {
        return [
            'roll_no' => [
                'source' => ['student_name', 'id'],
                'separator' => ''
            ]
        ];
    }

    /**
     * Get the phone associated with the user.
     */
    public function class()
    {
        return $this->hasOne(Classes::class, 'id', 'classes_id');
    }

    /**
     * Get the comments for the blog post.
     */
    public function subjects_score()
    {
        return $this->hasMany(StudentsScore::class, 'student_id', 'id')->select(array('student_id', 'english', 'maths', 'science', 'social_sciences', 'computer_basics'));
    }
}
