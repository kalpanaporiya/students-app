<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $table = 'classes';
    public $timestamps = false;
    protected $fillable = [
        'class_name'
    ];
}
