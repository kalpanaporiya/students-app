<?php

namespace App\Models;

use App\Models\Classes;
use App\Models\Students;

use Illuminate\Database\Eloquent\Model;

class StudentsScore extends Model
{
    protected $table = 'students_score';
    public $timestamps = false;
    protected $fillable = [
        'student_id', 'class_id', 'subject_id', 'score'
    ];

    /**
     * Get the phone associated with the user.
     */
    public function class()
    {
        return $this->hasOne(Classes::class, 'id', 'class_id');
    }

    /**
     * Get the phone associated with the user.
     */
    public function student()
    {
        return $this->hasOne(Students::class, 'id', 'student_id');
    }
}
