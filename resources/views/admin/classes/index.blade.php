@extends('admin.layouts.default')
@section('content')
<div class="content-wrapper-before gradient-45deg-indigo-purple cls-dashboard"></div>
@include('admin.includes.breadcrumbs')
<div class="col s12">
    <div class="container">
        <div class="section section-data-tables">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div id="button-trigger" class="card card card-default scrollspy">
                        <div class="card-content">
                            <h4 class="card-title">DataTables For Class Data</h4>
                            <div class="row">
                                <div class="col s12">
                                    <table id="dt-classes" class="display">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Class Name</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var routes = {
        'get_classes': `{{ route('admin.get.classes') }}`,
    };
</script>
<script src="{{ asset('js/classes.js') }}"></script>
@stop