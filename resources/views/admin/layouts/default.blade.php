<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <head>
        @include('admin.includes.head')
    </head>
    <body class="horizontal-layout page-header-light horizontal-menu preload-transitions 2-columns" data-open="click" data-menu="horizontal-menu" data-col="2-columns">
        @include('admin.includes.header')
        @include('admin.includes.sidebar')
        <div id="main">
            <div class="row">
                @yield('content')
            </div>
        </div>
        @include('admin.includes.footer')
        @include('admin.includes.footer_scripts')
    </body>
</html>