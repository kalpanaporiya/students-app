<!-- BEGIN: Footer-->
<footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
    <div class="footer-copyright">
        <div class="container"><span>&copy; 2020 <a href="javascript:void(0)" target="_blank">Student Management</a> All rights reserved.</span><span class="right hide-on-small-only">Developed by <a href="javascript:void(0)">Student Management </a></span></div>
    </div>
</footer>
