@extends('admin.layouts.default')
@section('content')
<div class="content-wrapper-before gradient-45deg-indigo-purple cls-dashboard"></div>
@include('admin.includes.breadcrumbs')
<!-- JQUERY VALIDATION -->
<div class="col s12">
    <div class="container">
        <div class="section section-data-tables">
            <div class="card">
                <div class="card-content">
                    <p class="caption mb-0">
                        <a class="btn waves-effect waves-light breadcrumbs-btn right modal-trigger" href="#md_students">
                            <i class="material-icons left">add_to_photos</i>Add New student
                        </a><br /><br />
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m12 l12">
                    <div id="button-trigger" class="card card card-default scrollspy">
                        <div class="card-content">
                            <h4 class="card-title">DataTables For Students Data</h4>
                            <div class="row">
                                <div class="col s12">
                                    <table id="dt-students" class="display">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Image</th>
                                                <th>Class</th>
                                                <th>Roll No.</th>
                                                <th>Student Name</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="md_students" class="modal modal-fixed-footer">
    <form class="formValidate" id="frm-students" method="post" action="{{ route('admin.add.students') }}" enctype="multipart/form-data">
        <div class="modal-content">
            <div class="row">
                <div class="col s12">
                    <h4>New Student</h4>
                </div>
            </div>
            <div id="view-validations">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="row">
                    <div class="col s12">
                        <label for="role">Class</label>
                        <div class="input-field">
                            <select class="error" id="classes_id" name="classes_id" required data-error=".classes_id">
                                <option value="">Choose Default class</option>
                                <?php
                                if (!empty($classes_data)) {
                                    foreach ($classes_data as $class) {
                                        echo '<option value="' . $class->id . '">' . $class->class_name . '</option>';
                                    }
                                }
                                ?>
                            </select>
                            <small class="classes_id"></small>
                        </div>
                    </div>
                    <div class="input-field col s12">
                        <label for="student_name">Student Name*</label>
                        <input id="student_name" value="<?php echo !empty($data->student_name) ? $data->student_name : '' ?>" name="student_name" type="text" data-error=".student_name">
                        <small class="student_name"></small>
                    </div>
                    <label class="input-field col s12" for="student_photo">Student's Image *</label>
                    <div class="input-field col s12">
                        <input id="input-file-now" value="<?php echo !empty($data->email) ? $data->email : '' ?>" type="file" class="dropify" name="student_photo" data-error=".student_photo">
                        <small class="student_photo"></small>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn waves-effect waves-light right submit" type="submit" name="action">Save
                <i class="material-icons right">send</i>
            </button>
            <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat" type="submit">Cancel</a>
        </div>
    </form>
</div>
<script>
    var routes = {
        'get_students': `{{ route('admin.get.students') }}`,
    };
    $(document).ready(function() {
        $('.modal').modal();
    });
</script>
<script src="{{ asset('assets/admin/vendors/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/students.js') }}"></script>
@stop