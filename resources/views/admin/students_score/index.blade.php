@extends('admin.layouts.default')
@section('content')
<div class="content-wrapper-before gradient-45deg-indigo-purple cls-dashboard"></div>
@include('admin.includes.breadcrumbs')
<!-- JQUERY VALIDATION -->
<div class="col s12">
    <div class="container">
        <div class="section section-data-tables">
            <div class="card">
                <div class="card-content">
                    <p class="caption mb-0">
                        <a class="btn waves-effect waves-light breadcrumbs-btn right modal-trigger" href="#md_students_score">
                            <i class="material-icons left">add_to_photos</i>Add New Score
                        </a><br /><br />
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m12 l12">
                    <div id="button-trigger" class="card card card-default scrollspy">
                        <div class="card-content">
                            <h4 class="card-title">DataTables For Students Score Data</h4>
                            <div class="row">
                                <div class="col s12">
                                    <table id="dt-students-score" class="display">
                                        <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Class</th>
                                                <th>Roll No.</th>
                                                <th>Student Name</th>
                                                <th>English</th>
                                                <th>Maths</th>
                                                <th>Science</th>
                                                <th>Social Sciences</th>
                                                <th>Computer Basics</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="md_students_score" class="modal modal-fixed-footer">
    <form class="formValidate" id="frm-students-score" method="post" action="{{ route('admin.add.students_scores') }}">
        <div class="modal-content">
            <div class="row">
                <div class="col s12">
                    <h4>Student Score</h4>
                </div>
            </div>
            <div id="view-validations">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="row">
                    <div class="col s6">
                        <label for="role">Class</label>
                        <div class="input-field">
                            <select class="error" id="student_id" name="student_id" required data-error=".student_id">
                                <option value="">Select student</option>
                                <?php
                                if (!empty($students_data)) {
                                    foreach ($students_data as $student) {
                                        echo '<option value="' . $student->id . '">' . $student->student_name . '</option>';
                                    }
                                }
                                ?>
                            </select>
                            <small class="student_id"></small>
                        </div>
                    </div>
                    <div class="input-field col s12"></div>
                    <div class="input-field col s4">
                        <label for="roll_no">Roll No*</label>
                        <input id="roll_no" name="roll_no" readonly type="text" data-error=".roll_no">
                        <small class="roll_no"></small>
                    </div>
                    <div class="input-field col s4">
                        <label for="student_name">Student Name*</label>
                        <input id="student_name" name="student_name" readonly type="text" data-error=".student_name">
                        <small class="student_name"></small>
                    </div>
                    <div class="input-field col s4">
                        <label for="classes_id">Class*</label>
                        <input id="classes_id" name="classes_id" readonly type="text" data-error=".classes_id">
                        <small class="classes_id"></small>
                    </div>
                    <div class="input-field col s6">
                        <label for="english">English *</label>
                        <input id="english" name="english" min="0" max="100" type="number" data-error=".english">
                        <small class="english"></small>
                    </div>
                    <div class="input-field col s6">
                        <label for="maths">Maths *</label>
                        <input id="maths" name="maths" type="number" data-error=".maths">
                        <small class="maths"></small>
                    </div>
                    <div class="input-field col s6">
                        <label for="science">Science *</label>
                        <input id="science" name="science" type="number" data-error=".science">
                        <small class="science"></small>
                    </div>
                    <div class="input-field col s6">
                        <label for="social_sciences">Social Sciences *</label>
                        <input id="social_sciences" name="social_sciences" type="number" data-error=".social_sciences">
                        <small class="social_sciences"></small>
                    </div>
                    <div class="input-field col s6">
                        <label for="computer_basics">Computer Basics*</label>
                        <input id="computer_basics" name="computer_basics" type="number" data-error=".computer_basics">
                        <small class="computer_basics"></small>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn waves-effect waves-light right submit" type="submit" name="action">Save
                <i class="material-icons right">send</i>
            </button>
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Cancel</a>
        </div>
    </form>
</div>
<script>
    var routes = {
        'get_studentdata': `{{ route('admin.get.studentdata') }}`,
        'get_student': `{{ route('admin.get.student') }}`,
    };
</script>
<script src="{{ asset('assets/admin/vendors/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/students_score.js') }}"></script>
@stop